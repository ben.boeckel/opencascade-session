//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#ifndef smtk_session_opencascade_ChildrenAs_txx
#define smtk_session_opencascade_ChildrenAs_txx

#include "smtk/session/opencascade/arcs/ChildrenAs.h"

#include "smtk/session/opencascade/Resource.h"
#include "smtk/session/opencascade/Session.h"

namespace smtk
{
namespace session
{
namespace opencascade
{

template <typename XToType>
bool ChildrenAs<XToType>::visit(
  const std::function<bool(const typename ChildrenAs<XToType>::ToType&)>& fn) const
{
  auto resource = static_pointer_cast<Resource>(m_from.resource());
  auto session = resource->session();
  TopoDS_Iterator it;
  for (it.Initialize(*m_from.data()); it.More(); it.Next())
  {
    auto uid = session->findID(it.Value());
    if (!uid)
    {
      continue;
    }
    auto node = dynamic_cast<ToType*>(resource->find(uid).get());
    if (!node)
    {
      continue;
    }
    if (fn(*node))
    {
      return true;
    }
  }
  return false;
}

template <typename XToType>
bool ChildrenAs<XToType>::visit(
  const std::function<bool(typename ChildrenAs<XToType>::ToType&)>& fn)
{
  auto resource = static_pointer_cast<Resource>(m_from.resource());
  auto session = resource->session();
  TopoDS_Iterator it;
  for (it.Initialize(*m_from.data()); it.More(); it.Next())
  {
    auto uid = session->findID(it.Value());
    if (!uid)
    {
      continue;
    }
    auto node = dynamic_cast<ToType*>(resource->find(uid).get());
    if (!node)
    {
      continue;
    }
    if (fn(*node))
    {
      return true;
    }
  }
  return false;
}
}
}
}

#endif
